<!DOCTYPE html>
<html lang="es">
<head>
<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
<meta name="description" content="Compra artiulos coreanos de tus grupos favoritos de kpop facil y sin tarjeta de credito">
<meta property="og:title" content="Comprar articulos coreanos"/>
<meta property="og:type" content="website"/>
<meta name="keywords" content="<?=$keyword?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
  <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.96.1/css/materialize.min.css">
	<!-- Latest compiled and minified CSS -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> -->

	<!-- Optional theme -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css"> -->
	<!-- Latest compiled and minified JavaScript -->
	
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		// echo $this->Html->meta('icon');

		echo $this->Html->css('style');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div class="container-fluid">
		<header >
			<div class="navbar-fixed">
			    <nav>
			      <div class="nav-wrapper">
			        <a href="#!" class="brand-logo">Mundo Oppa</a>
			        <ul class="right hide-on-med-and-down">
			          <li><a href="sass.html">Sass</a></li>
			          <li><a href="components.html">Components</a></li>
			          <li class="active"><a href="javascript.html">JavaScript</a></li>
			        </ul>
			      </div>
			    </nav>
			 </div>
		</header>
		<div class="row">
			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
			
		</div>
	</div>
 	<script>
//   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
//   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
//   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
//   })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

//   ga('create', 'UA-62032208-1', 'auto');
//   ga('send', 'pageview');

</script>
	<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>-->
</body>
</html>
