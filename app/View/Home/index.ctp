<div class="col s12 m4 l3"> <!-- Note that "m4 l3" was added -->
      <ul class="collection">
        <a href="#!" class="collection-item">Alan<span class="badge">1</span></a>
        <a href="#!" class="collection-item">Alan<span class="new badge">4</span></a>
        <a href="#!" class="collection-item">Alan</li>
        <a href="#!" class="collection-item">Alan<span class="badge">14</span></a>
      </ul>

</div>
<div class="col s12 m8 l9"> <!-- Note that "m8 l9" was added -->
<div class="row">
	<div class="col l4">
	  <div class="card">
	    <div class="card-image">
	      <img src="http://materializecss.com/images/sample-1.jpg">
	      <span class="card-title">Card Title</span>
	    </div>
	    <div class="card-content">
	      <p>I am a very simple card. I am good at containing small bits of information.
	      I am convenient because I require little markup to use effectively.</p>
	    </div>
	    <div class="card-action">
	      <a href="#">This is a link</a>
	      <a href='#'>This is a link</a>
	    </div>
	  </div>
	</div>
	<div class="col l4">
	  <div class="card">
	    <div class="card-image">
	      <img src="http://materializecss.com/images/sample-1.jpg">
	      <span class="card-title">Card Title</span>
	    </div>
	    <div class="card-content">
	      <p>I am a very simple card. I am good at containing small bits of information.
	      I am convenient because I require little markup to use effectively.</p>
	    </div>
	    <div class="card-action">
	      <a href="#">This is a link</a>
	      <a href='#'>This is a link</a>
	    </div>
	  </div>
	</div>
	<div class="col l4">
	  <div class="card">
	    <div class="card-image">
	      <img src="http://materializecss.com/images/sample-1.jpg">
	      <span class="card-title">Card Title</span>
	    </div>
	    <div class="card-content">
	      <p>I am a very simple card. I am good at containing small bits of information.
	      I am convenient because I require little markup to use effectively.</p>
	    </div>
	    <div class="card-action">
	      <a href="#">This is a link</a>
	      <a href='#'>This is a link</a>
	    </div>
	  </div>
	</div>
	<div class="col l4">
	  <div class="card">
	    <div class="card-image">
	      <img src="http://materializecss.com/images/sample-1.jpg">
	      <span class="card-title">Card Title</span>
	    </div>
	    <div class="card-content">
	      <p>I am a very simple card. I am good at containing small bits of information.
	      I am convenient because I require little markup to use effectively.</p>
	    </div>
	    <div class="card-action">
	      <a href="#">This is a link</a>
	      <a href='#'>This is a link</a>
	    </div>
	  </div>
	</div>
</div>
<!-- <div class="row">
	<div class="col l4">
	  <div class="card">
	    <div class="card-image">
	      <img src="http://materializecss.com/images/sample-1.jpg">
	      <span class="card-title">Card Title</span>
	    </div>
	    <div class="card-content">
	      <p>I am a very simple card. I am good at containing small bits of information.
	      I am convenient because I require little markup to use effectively.</p>
	    </div>
	    <div class="card-action">
	      <a href="#">This is a link</a>
	      <a href='#'>This is a link</a>
	    </div>
	  </div>
	</div>
	<div class="col l4">
	  <div class="card">
	    <div class="card-image">
	      <img src="http://materializecss.com/images/sample-1.jpg">
	      <span class="card-title">Card Title</span>
	    </div>
	    <div class="card-content">
	      <p>I am a very simple card. I am good at containing small bits of information.
	      I am convenient because I require little markup to use effectively.</p>
	    </div>
	    <div class="card-action">
	      <a href="#">This is a link</a>
	      <a href='#'>This is a link</a>
	    </div>
	  </div>
	</div>
	<div class="col l4">
	  <div class="card">
	    <div class="card-image">
	      <img src="http://materializecss.com/images/sample-1.jpg">
	      <span class="card-title">Card Title</span>
	    </div>
	    <div class="card-content">
	      <p>I am a very simple card. I am good at containing small bits of information.
	      I am convenient because I require little markup to use effectively.</p>
	    </div>
	    <div class="card-action">
	      <a href="#">This is a link</a>
	      <a href='#'>This is a link</a>
	    </div>
	  </div>
	</div>
</div> -->

</div>